const initialState = {
    products: [],
    error: '',
};

export const addProductReducer = (state = initialState, action) => {
    if (action.type === 'GET_PRODUCTS') {
        return { ...state, products: [...action.payload] };
    }
    if (action.type === 'FETCH_ERROR') {
        return { ...state, error: action.payload };
    }
    return state;
};

