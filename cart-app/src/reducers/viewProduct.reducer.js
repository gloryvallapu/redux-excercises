const initialState = {
    totalProducts: [],
    error: '',
};

export const viewProductReducer = (state = initialState, action) => {
    console.log('action123',action)
    if (action.type === 'GET_PRODUCTS') {
        return { ...state, totalProducts: [...action.payload] };
    }
    if (action.type === 'FETCH_ERROR') {
        return { ...state, error: action.payload };
    }
    return state;
}