
import { combineReducers } from "redux";
import { addProductReducer } from '../reducers/addProduct.reducer';
import { viewProductReducer } from '../reducers/viewProduct.reducer';
import {createStore,applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootSaga from '../sagas/sagas'
const sagaMiddleware = createSagaMiddleware()
const store = createStore(combineReducers({
    addProduct : addProductReducer,
    viewProducts: viewProductReducer}), applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);
export default store;
