
import React from 'react';
import { connect } from 'react-redux';
const Products = (props) => {

  const [pPrice, setPPrice] = React.useState(null);
  const [pName, setPName] = React.useState(null);
  const handlePname = (e) => {
    setPName(e.target.value);
  };
  const handlePprice = (e) => {
    setPPrice(e.target.value);
  };
  const addProduct = () => {
    // console.log('Add product button click');
    props.dispatch({ type: 'ADDED_NEW_PRODUCT', value: { pPrice, pName } });
  };
  return (
    <div>
      <h4>Add Product Form</h4>
      Product Name:
      <input type="text" onChange={handlePname} />
      <br />
      Product Price:
      <input type="text" onChange={handlePprice} />
      <br />
      <button onClick={addProduct}>Add Product</button>
    </div>
  );
};
export default connect((store) => {
  return store;
})(Products);
