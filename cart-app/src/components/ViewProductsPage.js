import React, { useEffect } from "react";
import { connect } from 'react-redux';
const ViewProduct = (props) => {
    console.log("lakki", props);
    useEffect(() => {
        props.dispatch({ type: 'GET_TOTAL_PRODUCTS' });
    }, []);

    console.log("pname", props.viewProducts.totalProducts)
    return (
        <div>
            <h1>Products Component</h1>
            {props.error && <h1>{props.error}</h1>}
            {props.viewProducts.totalProducts &&
                props.viewProducts.totalProducts.map((p, i) => {
                    return (
                        <div key={i}>
                            <li>
                                {p.pName}
                                <button
                                    onClick={() => {
                                        props.dispatch({ type: 'DEL_PRODUCT', id: p.id });
                                    }}
                                >
                                    Delete
                                </button>
                                <button>Edit</button>
                            </li>
                        </div>
                    );
                })}
        </div>
    );
}
export default connect((store) => {
    return store;
})(ViewProduct);