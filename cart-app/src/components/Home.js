import React from "react";
import Products from './AddProductsPage';
import ViewProduct from './ViewProductsPage';
const Home = () => {

    return (
        <div style={{ textAlign: "center" }}>
            <h1>Welcome to Home</h1>
            <Products />
            <ViewProduct />
        </div>
    )
}
export default Home;