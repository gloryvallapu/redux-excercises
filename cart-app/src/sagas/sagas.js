import { takeEvery, put, all } from 'redux-saga/effects';
import { getProducts, deleteProduct, addProduct } from '../api_urls';

export function* addProductWorker(x) {
    console.log("x", x);
    yield addProduct(x.value);
    yield put({ type: 'GET_TOTAL_PRODUCTS'});

}
export function* addProductWatcher() {
    yield takeEvery('ADDED_NEW_PRODUCT', addProductWorker);
}

function* getProductsWorker() {
    var products = yield getProducts();
    console.log("getproducts", products);
    if (products.error) {
        yield put({ type: 'FETCH_ERROR', payload: products.error });
    } else {
        yield put({ type: 'GET_PRODUCTS', payload: products });
    }
}
export function* getProductsWatcher() {
    yield takeEvery('GET_TOTAL_PRODUCTS', getProductsWorker);
}

export function* deleteProductWorker(x) {
    yield deleteProduct(x.id);
    yield put({ type: 'GET_PRODUCTS' });
}
export function* deleteProductWatcher() {
    yield takeEvery('DEL_PRODUCT', deleteProductWorker);
}

export default function* rootSaga() {
    yield all([
        getProductsWatcher(),
        deleteProductWatcher(),
        addProductWatcher(),
    ]);
}
