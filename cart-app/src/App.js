import './App.css';
import Home from './components/Home';
import { Provider } from 'react-redux';
import store from './store';
import { BrowserRouter, Link, Switch, Route } from 'react-router-dom';
import AboutUs from './components/About';
import ContactUs from './components/Contact';

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <div className="App">
          <h1>Welcome Cart Project</h1>
          <div style={{ border: "2px solid" }}>
            <Link to="/home">Home</Link>&nbsp;&nbsp;&nbsp;
            <Link to="/aboutus">About Us</Link>&nbsp;&nbsp;&nbsp;
            <Link to="/contactus">Contact Us</Link>
          </div>
        </div>
        <Switch>
          <Route path="/home">
            <Home />
          </Route>
          <Route path="/aboutus" component={AboutUs}></Route>
          <Route path="/contactus" component={ContactUs}></Route>
          <Route path="/" component={Home} exact={true}></Route>
        </Switch>
      </BrowserRouter>
    </Provider >
  );
}

export default App;
