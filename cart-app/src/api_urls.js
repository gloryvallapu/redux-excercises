// import axios from 'axios';

// const hostname = "http://localhost";
// const port = 3000;
// const apiUrl = `${hostname}:${port}/`;

// export function addProducts(obj) {
//     return axios.post(`${apiUrl}products`, obj)
//         // .then((res) => res.json())
//         // .catch(() => {
//         //     return Promise.resolve({ error: 'unable to add the records' });
//         // });
// }

// export function viewProducts() {
//     return axios.get(`${apiUrl}products`)
//         .then((res) => res.json())
//         .catch(() => {
//             return Promise.resolve({ error: 'unable to fetch the records' });
//         });
// }

// export function deleteProduct(id) {
//     return axios.delete(`${apiUrl}products/${id}`)
//         .then((res) => res.json())
//         .catch(() => {
//             return Promise.resolve({ error: 'unable to delete the records' });
//         });
// }


//-------------------------------------------------------------------------

import axios from 'axios';

const hostname = 'http://localhost';
const portnum = 3000;
const apiURL = `${hostname}:${portnum}/`;
export function getProducts() {
  return fetch(`${apiURL}products`)
    .then((res) => res.json())
    .catch(() => {
      return Promise.resolve({ error: 'unable to fetch records' });
    });
}

export function deleteProduct(id) {
  return axios.delete(`${apiURL}products/${id}`);
}

export function addProduct(obj) {
  return axios.post(`${apiURL}products`, obj);
}
