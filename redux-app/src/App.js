import './App.css';
import Counter from './components/counter';
import TodoList from './components/todo';
import LikeDislike from './components/likesDislikes';
import CartProject from './components/cartProject';
import Countries from './components/countries';
import { Provider } from 'react-redux';
import store from './store/store';

function App() {
    return ( <Provider store = { store } >
        <div className = "App" >
        <Counter />
        <TodoList />
        <LikeDislike />
        <CartProject />
        <Countries />
        </div> 
        </Provider>
    );
}

export default App;