import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { resetCart } from '../actions/cart.action';

const CartList = (props) => {

    // const [totalPrice, setTotalPrice] = useState(0);
    // const [discountedPrice, setDiscountedPrice] = useState(0);
  
    // useEffect(() => {
    //   let discountedPrice = 0;
    //   let total;
    //   let reducer = (previousValue, currentValue) => {
    //     discountedPrice = (currentValue.discountPrice / 100) * currentValue.price;
    //     previousValue.totalDiscount =
    //       previousValue.totalDiscount + discountedPrice;
    //     previousValue.totalAmount =
    //       previousValue.totalAmount + currentValue.price;
    //     previousValue.totalAfterDiscount =
    //       previousValue.totalAmount - previousValue.totalDiscount;
    //     return {
    //       totalAmount: previousValue.totalAmount,
    //       totalDiscount: previousValue.totalDiscount,
    //       totalAfterDiscount: previousValue.totalAfterDiscount,
    //     };
    //   };
  
    //   if (props.cart.cartProducts.length > 0) {
    //     total = props.cart.cartProducts.reduce(reducer, {
    //       totalAmount: 0,
    //       totalDiscount: 0,
    //       totalAfterDiscount: 0,
    //     });
    //     setTotalPrice(total.totalAfterDiscount);
    //     setDiscountedPrice(total.totalDiscount);
    //   } else {
    //     setTotalPrice(0);
    //     setDiscountedPrice(0);
    //   }
    // });

    const reset = () => {
        if (props.cart.cartProducts.length > 0) {
            props.dispatch(resetCart([]))
        }
    }

    return (
        <div style={{ border: '1px solid green', padding: '10px', margin: '10px' }}>
            <div>CartsItems : {props.cart.cartProducts.length}</div>
      {/* <div>Discounted price: {discountedPrice} </div>
      <div>TotalCost : {totalPrice}</div> */}
            <button onClick={() => reset()} > Reset </button>
        </div>
    )

};
export default connect((store) => store)(CartList);