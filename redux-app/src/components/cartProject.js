import React from 'react';
import { connect } from 'react-redux';
import CartList from './cart';
import ProductList from './productsList';

const CartProject =(props)=>{
return(
    <div style={{ border: '3px solid black', padding: '10px', margin: '10px' }}>
        <CartList />
        <ProductList />
    </div>
)};

export default connect((store)=>store)(CartProject);