import React from 'redux';
import { connect } from 'react-redux';
import { useState } from 'react';
const TodoList =(props)=>{
    const [addTodo, setAddTodo]=useState("");
    const handleChange =(e)=>{
        setAddTodo(e.target.value);
    }
    return(
        <div style={{border:'1px solid', margin:'2px', padding:'5px'}}>
            <h4>Todo List</h4>
            <input type="text" onChange={handleChange}/>
            <button
                onClick={()=>{
                    props.dispatch({type:'ADDTODO',newTodo:addTodo})
                }}
            >
                Add Todo</button>
            {
                <ul>
                    {props.todos.todoList.map((t,i)=>{
                        return <li key={i}>{t}</li>;
                    })}
                </ul>
            }
        </div>
    )
}

export default connect(store=>store)(TodoList);