import React from 'react';
import {connect} from 'react-redux';

const LikeDislike =(props)=>{
    return (
        <div style={{border:'1px solid', margin:'2px', padding:'5px'}}>
            <h4>Likes & Dislikes</h4>
            <h1>Likes:{props.likesDislikes.likes}    &nbsp;&nbsp;&nbsp; Dislikes:{props.likesDislikes.disLikes} </h1>
            <button 
            onClick={()=>{
                props.dispatch({type:'LIKES'})
            }}
            >
            Like
            </button>&nbsp;&nbsp;&nbsp;
            <button
             onClick={()=>{
                props.dispatch({type:'DISLIKES'})
            }}
            >
                DisLike</button>
        </div>
    )
}
export default connect(store=>{
    return store
})(LikeDislike);