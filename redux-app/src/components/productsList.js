import React from 'react';
import { connect } from 'react-redux';
import {addToCart, removeToCart} from '../actions/cart.action';

const ProductList =(props)=>{

   const addcart=(product)=>{
        props.dispatch(addToCart(product));
    }

    const removecart =(index)=>{
        if (props.cart.cartProducts.length > 0) {
            props.cart.cartProducts.splice(index, 1);
            props.dispatch(removeToCart(props.cart.cartProducts));
        }
    } 

    return(
        <div style={{ border: '1px solid red', padding: '10px', margin: '10px' }}>
            {props.products.products.map((p,i)=>{
                return (
                    <li>
                        {p.itemname}
                        <button onClick={()=>{addcart(p)}}>Add Product</button>
                        <button onClick={()=>{removecart(p)}}>Remove Product</button>
                    </li>
                )
            })}
        </div>
    )

};

export default connect((store)=>store)(ProductList);