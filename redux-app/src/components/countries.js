import React from 'react';
import { connect } from 'react-redux';
import { getCountries } from '../actions/countries.action';

const Countries = (props) => {

  return (
    <div style={{ border: '1px solid', padding: '10px', margin: '10px' }}>
      <h4>Countries</h4>
      <button onClick={() => {
        props.dispatch(getCountries());
      }}>
        Get All Countries
      </button>
      {/* {
        props.countries && props.countries.countries.map((c) => {
          return (
          <li>{c}</li>
          )
        })
      } */}
    </div>
  );
};

export default connect(store => {
  return store;
})(Countries);