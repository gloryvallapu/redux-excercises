import React from 'redux';
import {connect} from 'react-redux';
const Counter =(props)=>{
    return(
        <div style={{border:'1px solid', margin:'2px', padding:'5px'}}>
            <h4>Counter</h4>
            <h4>{props.counter.count}</h4>
            <button
             onClick={()=>{
                props.dispatch({type:'inc'})
            }}
            >
                increment
            </button>
            <button
             onClick={()=>{
                props.dispatch({type:'dec'})
            }}
            >
                decrement
            </button>
        </div>
    )
}
export default connect(store=>{
    return store;
})(Counter);