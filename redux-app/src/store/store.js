import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers/index';
const store = new createStore(rootReducer, applyMiddleware(thunk));

export default store;