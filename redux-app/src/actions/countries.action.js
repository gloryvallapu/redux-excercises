// import axios from "axios";

export const getCountries = (payload) => {
    // return { type: 'GETCOUNTRIES', payload: payload }
    return (dispatch) => {
        fetch("https://restcountries.eu/rest/v2/all")
            .then((res) => res.json())
            .then((data) => {
                dispatch({
                    type: 'GETCOUNTRIES',
                    payload: data
                })
            })

        // return axios.get("https://restcountries.eu/rest/v2/all")
        //     .then(({ data }) => {
        //         dispatch(setArticleDetails(data));
        //     });
    }
}