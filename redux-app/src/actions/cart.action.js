export const addToCart = (payload) => {
    return { type: 'ADDPRODUCT', payload: payload }
}

export const removeToCart = (payload) => {
    return { type: 'REMOVEPRODUCT', payload: payload }
}

export const resetCart = (payload) => {
    return { type: 'RESET', payload: payload }
}