const initialState ={
    count : 100
}

export const counterReducer = (state=initialState, action)=>{
    if(action.type==='inc'){
        return{count:state.count+1}
    }
    if(action.type==='dec'){
        return{count:state.count-1}
    }
    return state
}