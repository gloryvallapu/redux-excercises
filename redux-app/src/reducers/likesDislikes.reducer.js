const initialState={
        likes:0,
        disLikes:0
}

export const likesReducer = (state=initialState, action)=>{
    if(action.type==='LIKES'){
        return{likes:state.likes+1}
    }
    if(action.type==='DISLIKES'){
        return{disLikes:state.disLikes+1}
    }
    return state
}