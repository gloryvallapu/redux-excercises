import { combineReducers } from 'redux';
import { counterReducer } from '../reducers/counter.reducer';
import { todoReducer } from '../reducers/todo.reducer';
import { likesReducer } from '../reducers/likesDislikes.reducer';
import { cartReducer } from '../reducers/cart.reducer';
import { productReducer } from '../reducers/product.reducer';
import { countriesReducer } from '../reducers/countries.reducer';

export default combineReducers({
    counter: counterReducer,
    todos: todoReducer,
    likesDislikes: likesReducer,
    cart: cartReducer,
    products: productReducer,
    countries: countriesReducer,
});