const initialState = {
    cartProducts: [],
}

export const cartReducer = (state=initialState, action)=>{
    if(action.type==='ADDPRODUCT'){
        return{
            ...state,
            cartProducts:[...state.cartProducts, action.payload]
        }
    }
    if(action.type==='REMOVEPRODUCT'){
        return{
            ...state,
            cartProducts:action.payload
        }
    }
    if(action.type==='RESET'){
        return{
            ...state,
            cartProducts:initialState.cartProducts,
        }

    }

    return state
}
