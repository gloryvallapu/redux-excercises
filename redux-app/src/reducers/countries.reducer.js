const initialState = {
    countriesList: []
}

export const countriesReducer = (state = initialState, action) => {
        if (action.type === "GETCOUNTRIES") {
            return {...state, countriesList: [...action.payload] }
        }
        return state;
    }
    // export default countriesReducer;